<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Post([
          'user_id' =>1,// $request->get(''),
          'title' => $request->get('title'),
          'slug' => $request->get('slug'),
          'body' => $request->get('body'),
          'active' => $request->has('active'),
          'published_at' => $request->get('published_at')
        ]);
        $item->save();
        return response()->json('Successfully added');
    }

    public function get(Request $request)
    {
         $item= Post::find(1);
         return response()->json($item);
    }
}
